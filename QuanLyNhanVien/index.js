var dsnv = [];
// lấy localstorage
var dsnvLocalStorage = localStorage.getItem("dsnv");
if (JSON.parse(dsnvLocalStorage)) {
  var dataNv = JSON.parse(dsnvLocalStorage);
  dataNv.forEach(function (dsnvItem) {
    var nv = new nhanVien(
      dsnvItem.taiKhoan,
      dsnvItem.ten,
      dsnvItem.email,
      dsnvItem.matKhau,
      dsnvItem.ngayLam,
      dsnvItem.luong,
      dsnvItem.ValueChucvu,
      dsnvItem.gioLam
    );
    dsnv.push(nv);
  });
  inDsnv(dsnv);
}
function btnThemNV() {
  var newNV = layThongTin();
  // if (
  //   validator.kiemTraRong(newNV.taiKhoan, "spanTaiKhoan", "không được rỗng") &&
  //   validator.kiemTraEmail(newNV.email, "spanEmail", "không được") &&
  //   validator.kiemTraTen(newNV.ten, "spanName")
  // ) {
  //   dsnv.push(newNV);
  //   renderDsNV(dsnv);
  // }

  // validate tài khoản Nv
  var checkUser = validator.kiemTraRong(
    newNV.taiKhoan,
    "spanTaiKhoan",
    "Tài khoản chưa nhập"
  );
  // validate tên  Nvss

  var checkName =
    validator.kiemTraRong(newNV.ten, "spanName", " Tên chưa nhập") &&
    validator.kiemTraTen(newNV.ten, "spanName", "Tên phải là chữ");
  // validate email  Nv

  var checkMail =
    validator.kiemTraRong(newNV.email, "spanEmail", " Email chưa nhập") &&
    validator.kiemTraEmail(newNV.email, "spanEmail", "Email không hợp lệ");
  // validate mật khẩu  Nv

  var checkPass =
    validator.kiemTraRong(newNV.matKhau, "spanMk", " Mật khẩu chưa nhập") &&
    validator.kiemTraDoDai(newNV.matKhau, "spanMk", 6, 10) &&
    validator.kiemTraKiTu(newNV.matKhau, "spanMk");

  var checkDay =
    validator.kiemTraRong(newNV.ngayLam, "spanDay", " Ngày làm chưa nhập") &&
    validator.kiemTraTime(newNV.ngayLam, "spanDay");
  // validate lương Nv

  var checkSala =
    validator.kiemTraRong(newNV.luong, "spanMoney", " Lương chưa nhập") &&
    validator.kiemTraSalary(newNV.luong, "spanMoney");
  // validate Giờ làm Nv

  var checkTime =
    validator.kiemTraRong(newNV.gioLam, "spanHour", " Giờ làm chưa nhập") &&
    validator.kiemTraThoiGian(newNV.gioLam, "spanHour");

  // validate Chức vụ Nv

  var checkRole = validator.kiemTraChucVu(newNV.chucVu, "spanType");

  if (
    checkDay &&
    checkMail &&
    checkName &&
    checkPass &&
    checkRole &&
    checkTime &&
    checkSala &&
    checkUser
  ) {
    dsnv.push(newNV);

    renderDsNV(dsnv);
  }
}

// xoá nhân viên
function xoaNv(id) {
  var index = timKiemViTri(id, dsnv);
  if (index !== -1) {
    dsnv.splice(index, 1);

    renderDsNV(dsnv);
  }
}
// sửa nhân viên
function suaNv(id) {
  document.getElementById("btnThemNv");
  var index = timKiemViTri(id, dsnv);
  if (index !== -1) {
    var nv = dsnv[index];
    showThongTinLenForm(nv);
  }
}
