function layThongTin() {
  var taiKhoan = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var gioLam = document.getElementById("gioLam").value;
  var luong = document.getElementById("luongCB").value * 1;

  console.log(tongLuong);

  //cách 1: sử dụng if else để lấy ra tên chức vụ.
  // var chucVu = document.getElementById("chucvu").value;
  // var tenChucVu;
  // if (chucVu == 3) {
  //   tenChucVu = "Sếp";
  // } else if (chucVu == 2) {
  //   tenChucVu = "Trưởng Phòng";
  // } else if (chucVu == 1) {
  //   tenChucVu = "Nhân Viên";
  // }

  //cách 2: sử dụng js dom tới text của option luôn
  var chucVu =
    document.getElementById("chucvu").options[
      document.getElementById("chucvu").selectedIndex
    ].value;

  var tongLuong;
  if (chucVu == 3) {
    tongLuong = luong * 3;
  } else if (chucVu == 2) {
    tongLuong = luong * 2;
  } else {
    tongLuong = luong;
  }
  var xepLoai;
  if (gioLam >= 192) {
    xepLoai = "Nhân viên Xuất Sắc";
  } else if (gioLam >= 176) {
    xepLoai = " Nhân viên giỏi";
  } else if (gioLam >= 160) {
    xepLoai = " Nhân viên khá";
  } else {
    xepLoai = "Nhân viên trung bình";
  }
  return {
    taiKhoan: taiKhoan,
    ten: ten,
    email: email,
    matKhau: matKhau,
    ngayLam: ngayLam,
    chucVu: chucVu,
    gioLam: gioLam,
    luong: tongLuong,
    xepLoai,
  };
}

function renderDsNV(list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var content = ` 
        <tr>
        <td>${item.taiKhoan}</td> 
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.ngayLam}</td>
        <td>${item.chucVu}</td>
        <td>${item.gioLam}</td>
        <td>${item.luong}</td>
        <td>${item.xepLoai}</td>
        <td>
        <button class="btn btn-danger" onclick="xoaNv('${item.taiKhoan}')" >Xoá</button>
        <button class="btn btn-warning" onclick="suaNv('${item.taiKhoan}')" >Sửa</button>
        </td>
        </tr>`;
    contentHTML += content;
  });

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function timKiemViTri(id, arr) {
  for (var index = 0; index < arr.length; index++) {
    var Nv = arr[index];
    if (Nv.taiKhoan == id) {
      return index;
    }
  }
  return -1;
}
function showThongTinLenForm(Nv) {
  document.getElementById("tknv").value = Nv.taiKhoan;
  document.getElementById("name").value = Nv.ten;
  document.getElementById("email").value = Nv.email;
  document.getElementById("password").value = Nv.matKhau;
  document.getElementById("datepicker").value = Nv.ngayLam;
  document.getElementById("luongCB").value = Nv.luong;
  document.getElementById("chucvu").value = Nv.chucVu;
  document.getElementById("gioLam").value = Nv.gioLam;
}
