var validator = {
  kiemTraRong: function (valueInput, idError, message) {
    if (valueInput == "") {
      domID(idError).innerText = message;
      return false;
    } else {
      domID(idError).innerText = "";
      return true;
    }
  },

  kiemTraDoDai: function (valueInput, idError, min, max) {
    var inputLength = valueInput.length;
    if (inputLength < min || inputLength > max) {
      domID(idError).innerText = `Độ dài phải từ${min}-${max} kí tự`;
      return false;
    } else {
      return true;
    }
  },
  kiemTraKiTu: function (valueInput, idError) {
    var strongRegex = new RegExp(
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{6,})"
    );
    if (strongRegex.test(valueInput)) {
      domID(idError).innerText = "";
      return true;
    } else {
      domID(
        idError
      ).innerText = `Phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`;
      return false;
    }
  },

  kiemTraEmail: function (valueInput, idError) {
    var filter =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(valueInput)) {
      domID(idError).innerText = "";
      return true;
    } else {
      domID(idError).innerText = `Email không hợp lệ.`;
      return false;
    }
  },
  kiemTraTen: function (valueInput, idError) {
    var regex =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (regex.test(valueInput)) {
      domID(idError).innerText = ``;
      return true;
    } else {
      domID(idError).innerText = `Chỉ được nhập chữ`;
      return false;
    }
  },

  kiemTraTime: function (valueInput, idError) {
    var regax =
      /\b(0?[1-9]|[12]\d|3[01])[\/\-.](0?[1-9]|[12]\d|3[01])[\/\-.](\d{2}|\d{4})\b/;
    if (regax.test(valueInput)) {
      domID(idError).innerText = ``;
      return true;
    } else {
      domID(idError).innerText = ` Phải đúng định dạng mm/dd/yyyy `;
      return false;
    }
  },
  kiemTraSalary: function (valueInput, idError) {
    if (valueInput < 1e6 || valueInput > 20e6) {
      domID(idError).innerText = "Lương cơ bản từ 1.000.000 -20.000.000";
      return false;
    }
    return true;
  },

  kiemTraChucVu: function (valueInput, idError) {
    //  if (valueInput) {
    if (valueInput !== "Chọn chức vụ") {
      //đk sử dụng cách 2
      domID(idError).innerText = "";
      return true;
    } else {
      domID(idError).innerText = "Chưa chọn chức vụ";
      return false;
    }
  },
  kiemTraThoiGian: function (valueInput, idError) {
    if (valueInput < 80 || valueInput > 200) {
      domID(idError).innerText = "Giờ làm phải từ 80-200 giờ";
      return false;
    }
    return true;
  },
};
